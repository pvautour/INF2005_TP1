<!DOCTYPE html>
<html lang="FR">

<head>
    <meta charset="utf-8">
    <title>Boutique de livres - TP1</title>
</head>

<body background="http://www.planwallpaper.com/static/images/colorful-triangles-background_yB0qTG6.jpg">
    <h1 align="center">Bienvenu</h1>
    <h2>Table des matieres</h2>
    <?php
        $biblio = fopen("./biblio.txt", "r");
        echo "<ul>";
        while (($ligne = fgets($biblio)) !== false) {
            echo "<li><a href=\"#" . trim($ligne) . "\">";
            $ligne = fgets($biblio);
            echo $ligne;
            echo "</a></li>";
            while (($ligne = fgets($biblio)) !== false && trim($ligne) !== "* * *") {
            }
        }
        echo "</ul>";
        echo "<br>";
        fclose($biblio);
        ?>
        <h2 align="center">Livres</h2>
        <table border="21px solid black">
            <thead>
                <th>Identifiant</th>
                <th>Titre</th>
                <th>Auteur(s)</th>
                <th>Annee de publication</th>
                <th>Maison d'edition</th>
                <th>Nombre de copies en inventaire</th>
                <th>Acheter</th>
                <th>Photo</th>
            </thead>
            <tbody align="center">
                <?php
                $biblio = fopen("./biblio.txt", "r");
                while (($ligne = fgets($biblio)) !== false) {
                    echo "<tr>";
                    echo "<td>" . $ligne . "<div id=\"" . trim($ligne) . "\"></div>" . "</td>";
                    $ligne = fgets($biblio);
                    echo "<td>" . $ligne . "</td>";
                    $ligne = fgets($biblio);
                    echo "<td>" . $ligne . "</td>";
                    $ligne = fgets($biblio);
                    echo "<td>" . $ligne . "</td>";
                    $ligne = fgets($biblio);
                    echo "<td>" . $ligne . "</td>";
                    $ligne = fgets($biblio);
                    echo "<td>" . $ligne . "</td>";
                    $ligne = fgets($biblio);
                    echo "<td><a href=\"" . $ligne . "\"><img style=\"width:28px;height:28px;\" src=\"https://cdn3.iconfinder.com/data/icons/glypho-free/64/shopping-purse-512.png\"></img></a></td>";
                    $ligne = fgets($biblio);
                    echo "<td><img style=\"width:128px;height:128px;\" alt=\"Aucune image\" src=\"" . $ligne . "\"></img></td>";
                    $ligne = fgets($biblio);
                    echo "</tr>";
                }
                fclose($biblio);
                ?>
            </tbody>
        </table>
</body>
</html>